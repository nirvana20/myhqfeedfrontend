jQuery(document).ready(function() {

  $(document).on('click','#selected_value li',function() {
      // code for post type selection(trending,popular,recent)
    var current_text = $(this).text();
    var sort_by = '' 
    if (current_text == 'Top recent') {
        sort_by = 'timestamp'
    } else if(current_text == 'Most Popular') {
        sort_by = 'popularity'
    } else if (current_text ==  'Most Trending') {
        sort_by = 'engagementrate'
    }
    $('#post_type').text($(this).text());
    $('#post_type').val(sort_by)
    createPosts(sort_by)  
  });
  $(document).on('click','.pages',function() {
    // pagination code 
    var current_page_clicked = $(this);
    $(".active_page").removeClass("active_page");
        current_page_clicked.addClass("active_page");
        var sort_by = $('#post_type').val();
        createPosts(sort_by);
      
  });   
  $(document).on('click','.like_click',function() {  
        //code for liking a post
    var clicked_data = $(this);
    var post_id = $(this).attr('post_id');
    var user_id = localStorage.getItem("user_id");
    $.ajax({
      url: URLROOT+'posts/'+post_id+'/like',
      type: 'post',
      data: JSON.stringify({'userId': user_id, "postId": post_id}),
      dataType: 'json',
      contentType: 'application/json',
      success: function (data) {
        clicked_data.text(data['likes'].length+' Like');
      }, error: function(data){
          console.log(error);
          alert(error);
      }
    });
  });
  $(document).on('click','.comment_submit',function(){ 
          //code for commenting on a post
    var button_data= $(this);
    var post_id = button_data.attr('post_id')
    if (!$('#id_'+post_id).val()) {
      alert('please enter comment');
      console.log('please enter comment');
    } else {
        var userId = localStorage.getItem("user_id");
        var content = $('#id_'+post_id).val()
        var data = {"userId": userId, "content":content,"postId":post_id}
        $.ajax({
          url: URLROOT+'posts/'+post_id+'/comment',
          type: 'post',
          data: JSON.stringify(data),
          dataType: 'json',
          contentType: 'application/json',
          success: function (data) {
            location.reload()
          }, error: function(error){
              alert(error)
              console.log(error)
          }

          });
        }
    });

  function createPosts(sort_by){
    //code for geting post
    var post_type = $('#post_type').val()
    var page_no = $(".active_page").text();
    if (!localStorage.getItem("user_id")) {
      window.location = "login.html"
    }
    if ($(".post_status")) {
      $(".post_status").remove();
    } 
    userID = localStorage.getItem("user_id");
    $.ajax({
      url: URLROOT+'posts/'+page_no+'/10/' +sort_by,
      type: 'get',
      dataType: 'json',
      contentType: 'application/json',
      success: function (data) {
        console.log(data)
        for (var i = 0; i < data.length; i++) {
          // Creating post from json response from server 
          var div = $(".gedf-main")
          var child_div = $("<div>").addClass("card gedf-card post_status")
          var child_div_child = $("<div>").addClass("card-header")
          child_div_child.text("Author")
          author_name_div = $("<div>").addClass("h7 text-muted").text(data[i]['user']['name'])
          author_name_div.appendTo(child_div_child)
          child_div_child.appendTo(child_div)
          child_div.appendTo(div)
          var card_body_div = $("<div>").addClass("card-body")
          var created_at = new Date(data[i]['createdAt'])
          var date_tostring = created_at.toString()
          var date_extracted = date_tostring.split(" GMT")
          var time_body_div = $("<div>").addClass("text-muted h7 mb-2").text(date_extracted[0])
          time_body_div.appendTo(card_body_div)
          var blog_link = $("<a>").addClass("card-link")
          var blong_link_text = $("<h5>").addClass("card-title").text("Article Content")
          blong_link_text.appendTo(blog_link)
          blog_link.appendTo(card_body_div)
          var blog_text = $("<p>").addClass("card-text").text(data[i]['content'])
          blog_text.appendTo(card_body_div)
          var footer_div = $("<div>").addClass("card-footer")
          var like_link = $("<a>").addClass("card-link like_click").attr('href', 'javascript:void(0)').text(data[i]['likes'].length+' Like')
          like_link.attr("post_id", data[i]['_id'])
          var  comment_link = $("<a>").addClass("card-link comments_click").attr('href', 'javascript:void(0)').text(data[i]['comments'].length +' Comment')
          comment_link.attr("post_id", data[i]['_id'])
          like_link.appendTo(footer_div)
          comment_link.appendTo(footer_div)
          card_body_div.appendTo(child_div)
          var comment_area = $("<div>").addClass("form-group")
          var comment_text = $("<textarea>").addClass("form-control").attr('rows','2')
          comment_text.attr('placeholder', 'comment')
          comment_text.attr('id', 'id_'+data[i]['_id'])
          comment_text.appendTo(comment_area);
          var button_comment= $("<button>").addClass("btn btn-primary comment_submit")
          button_comment.attr('post_id',data[i]['_id'])
          button_comment.text('comment')
          button_comment.appendTo(comment_area);
          comment_area.appendTo(footer_div);
          for(j=0;j<data[i]['comments'].length;j++){
            var comment_content = data[i]['comments'][j]['content']
            var comment_content_div = $("<div>").addClass("form-group")
            var comment_content_p = $("<p>").addClass("form-control").attr('rows','1')
            comment_content_p.text(comment_content+'- commented by -'+data[i]['comments'][j]['user']['name'])
            comment_content_p.appendTo(comment_content_div)                            
            comment_content_div.appendTo(footer_div)           
          }
          footer_div.appendTo(child_div);

        }
      }, error: function(error) {
          alert(error)
          console.log(error);
      }
    });


}
createPosts('timestamp')

  $(document).on('click','#post_content',function(){ 
    // code for posting an article
    var content = $('textarea#posts_data').val()
    if(!content){
      console.log('add content for data to posts')
      alert('add content for data to posts')
    } else{
        user_id = localStorage.getItem("user_id");
        console.log('posting data')
        var data = {'userId': user_id, "content": content}
        $.ajax({
          url: URLROOT+'posts/',
          type: 'post',
          data: JSON.stringify(data),
          dataType: 'json',
          contentType: 'application/json',
          success: function (data) {
              location.reload();
          },
          error: function(error) {
             alert(error)
             console.log(error);
          }
        });
      }
  });
})  