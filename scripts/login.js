//Posting authentcation details for authorization
jQuery(document).ready(function(){
   $('.form-horizontal').on('submit', function (e) {
      e.preventDefault();
      $.ajax({
        type: 'post',
        url: URLROOT+'login/',
        data: $('form').serialize(),
        success: function (data) {
          localStorage.setItem("user_id", data['_id']);
          window.location = "blog_list.html";
        }
      });
  });
});